export const userSubmissionDataDecode = ( data ) => {
    
    return new Promise(resolve => {
        
        const paylodData = {

            name  : data.name,
            uname : data.uname, 
            verdictCount : {    
                AC  : 0, WA  : 0, TLE : 0, RE  : 0,
                CE  : 0, ML  : 0, PE  : 0, OL  : 0
            },
            totalSolve : 0,
            eachYearSolveCount : []
        }

        let minYear = 300000;
        let maxYear = 0;
        let curretYear = new Date().getFullYear();
        let solveCounts = {};
        let uniqueSolveProblemList = new Set();

        for(const sub of data.subs) {

            switch(sub[2]){
                case 90: 
                    paylodData.verdictCount.AC++;
                    break;
                case 70: 
                    paylodData.verdictCount.WA++; 
                    break; 
                case 50: 
                    paylodData.verdictCount.TLE++; 
                    break;
                case 40: 
                    paylodData.verdictCount.RE++; 
                    break;
                case 30: 
                    paylodData.verdictCount.CE++; 
                    break; 
                case 60: 
                    paylodData.verdictCount.ML++; 
                    break; 
                case 80:  
                    paylodData.verdictCount.PE++; 
                    break; 
                case 45: 
                    paylodData.verdictCount.OL++; 
                    break; 
                default : 
                    break; 
            }

            let yearTheProblemSubmited = new Date(sub[4] * 1000).getFullYear();

            minYear = Math.min(minYear , yearTheProblemSubmited);
            maxYear = Math.max(maxYear , yearTheProblemSubmited);

            if(sub[2] === 90 && !uniqueSolveProblemList.has(sub[1])){

                solveCounts.hasOwnProperty(yearTheProblemSubmited) ? solveCounts[yearTheProblemSubmited]++ :  solveCounts[yearTheProblemSubmited] = 1;
                uniqueSolveProblemList.add(sub[1]);
            }  
        }
        paylodData.totalSolve = uniqueSolveProblemList.size;

        solveCounts[minYear - 1] = 0;

        while(maxYear < curretYear) {
            maxYear++;
            solveCounts[maxYear] = 0;
        }
        
        for(const key in solveCounts){
            paylodData.eachYearSolveCount.push({key : key , solved : solveCounts[key]});
        }
        
        for(let i = 1 ; i < paylodData.eachYearSolveCount.length ; ++i){
            paylodData.eachYearSolveCount[i].solved += paylodData.eachYearSolveCount[i - 1].solved;
        }

        resolve(paylodData);
    })

}


export const findProblemByProblemId = (problemId) => {

    const allProblemsList = JSON.parse(localStorage.getItem("allProblemList"));

    let low = 0  , high = allProblemsList.length , mid = 0;
    
    while(low <= high){

        mid = (low + high) >> 1;

        if(allProblemsList[mid][0] === problemId) return mid;
        else if(allProblemsList[mid][0] < problemId)low = mid + 1;
        else high = mid - 1;
    }
    
    return 0;
}