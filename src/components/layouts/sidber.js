import React from 'react'
import { Layout, Menu } from "antd";

import {

    UserOutlined,
    SwapOutlined,
    SlidersFilled,
    AppstoreFilled,
    CrownOutlined,
    TrophyOutlined,
    VideoCameraOutlined,

} from "@ant-design/icons";

import { Link } from 'react-router-dom';

const { SubMenu } = Menu;

const { Sider } = Layout;

const MySidber = ( ) => {
    
    const collapsed = false;
    
    return(
        
        <Sider width={256} >

            <div className="logo mb-3" >
                <h3> Uva <span className="color-text"> Holder </span> </h3>
            </div>
            
            <Menu theme="dark" mode="inline" defaultSelectedKeys={[ "1" ]} >
                <Menu.Item key="1" icon={<UserOutlined />}> 
                    <span> Dashboard </span>
                    <Link to="/dashboard" />
                </Menu.Item>
                
                <Menu.Item key="2" icon={<VideoCameraOutlined />}>  
                    <span>Live Submissions</span>
                    <Link to="/live-submissions" /> 
                </Menu.Item>
                
                <SubMenu key="problem" icon={<AppstoreFilled />} title="Problems">
                    
                    <Menu.Item key="3"  >
                        <span> All </span> 
                        <Link to="/problems" />
                    </Menu.Item>
                    
                    <Menu.Item key="4" >
                        <span> Search </span> 
                        <Link to="/problems/search" />
                    </Menu.Item>

                </SubMenu>

                <SubMenu key="submission" icon={<SlidersFilled />} title="Your Submissions">
                    
                    <Menu.Item key="5" > 
                        <span>Latest</span> 
                        <Link to="/" />
                    </Menu.Item>
                    
                    <Menu.Item key="6" >
                        <span>Search</span>  
                        <Link to="/" />
                    </Menu.Item>
                    
                    <Menu.Item key="7" >
                        <span>Tried</span>     
                        <Link to="/" />
                    </Menu.Item>

                </SubMenu>

                <Menu.Item key="8" icon={<SwapOutlined />} >
                    <span> Statistics Comparer </span>
                    <Link to="/compare" />
                </Menu.Item>

                <Menu.Item key="9" icon={<SwapOutlined />} >
                    <span> CP Book </span>
                    <Link to="/cp-book" />
                </Menu.Item>

                <Menu.Item key="10" icon={<TrophyOutlined />} >
                    <span> Contest </span>
                    <Link to="/contest" />
                </Menu.Item>
                
                <Menu.Item key="11" icon={<CrownOutlined />} >
                    <span> Rank List </span>  
                    <Link to="/rank-list" />
                </Menu.Item>

            </Menu>
        </Sider>
    );
}


export default MySidber;