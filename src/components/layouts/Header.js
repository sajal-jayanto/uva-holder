import React from 'react'
import { Button, Layout } from "antd";
import { LogoutOutlined } from '@ant-design/icons';

const { Header } = Layout;

const MyHeader = () => {

    return(
        <Header className="site-layout-background" >
            <Button style={{ float:'right' , margin : 12}} type="primary" shape="round" icon={<LogoutOutlined />} size='large'>
                Logout
            </Button>
        </Header>
    );
}

export default MyHeader;

