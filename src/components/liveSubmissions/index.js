import React , { useState,  useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { connect } from 'react-redux';
import { Card  } from 'antd';
import Loding from '../shared/loding';
import { liveSubmissionsAction } from '../../redux/liveSubmissions/liveSubmissionsAction';


const LiveSubmissions = ({ liveSumbissions , allproblemList }) => {
    
    const dispatch = useDispatch();
    const [ pullId , setPullId ] = useState(0);
    const [ samePullId , setSamePullId ] = useState(0);

    useEffect(() => {

        const fatchData = async () => {

            const response = await liveSubmissionsAction(dispatch , pullId);            
            if(pullId == response) setSamePullId(samePullId + 1);
            else setPullId(response);
        }

        if(samePullId >= 100000) setSamePullId(0);
        fatchData();

    }, [dispatch , pullId , samePullId]);

    

    return(

        <React.Fragment>
            
          
                
            <Card className="card-hader" title="Live Submissions" bordered={true}   
                headStyle={{   
                    background: '#1890ff' , 
                    textTransform: 'uppercase', 
                    color: '#fff' 
                }}
            >  

                <table className="table table-sm table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Problem</th>
                            <th scope="col">User (User Name)</th>
                            <th scope="col">Verdict</th>
                            <th scope="col">Language </th>
                            <th scope="col">Time</th>
                            <th scope="col">Best</th>
                            <th scope="col">Submission Time </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            
                        </tr>
                    </tbody>
                </table>
                {/* <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className="page-item">
                            <a className="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li className="page-item"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item">
                            <a className="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav> */}

            </Card> 
            

        </React.Fragment>
    );
};


const mapStateTopProps = state => { 
    return {
        
        liveSumbissions : state.liveSubmissionsData

    };
}

export default connect(mapStateTopProps , null)(LiveSubmissions);