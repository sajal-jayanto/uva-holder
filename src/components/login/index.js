import React , { useState , useEffect } from 'react';
import { useDispatch } from 'react-redux';
import '../../styles/login.css';
import LoginBg from '../../assets/images/login-bg.jpg';
import { Input , Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { loginAction } from '../../redux/login/loginAction';
import { notificationWithIcon } from '../../util/notification';
import { useHistory } from "react-router-dom";
import { allProblemlListAction } from '../../redux/allProblemList/allProblemListAction';



const bgStyle = {
    backgroundImage : `url(${LoginBg})`,
    width: "100%",
    backgroundPosition: 'top center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    position:'relative',
    left:'-2px'
}


const Login = () => {

    const [ userName , setUserName ] = useState("");
    const dispatch = useDispatch();
    const history = useHistory();
    
    const submitUserName = async () => {
        const response = await loginAction(dispatch, userName);
        await allProblemlListAction(dispatch);

        if(response) history.push('/dashboard');
        else notificationWithIcon('error' , 'Invalid Username' , `No user found name ${userName}`);  

    }

    

    return (
        <React.Fragment>
            <div className="container-fluid no-padding no-margin">
                <div className="row no-gutters">
                    <div className="col-lg-8 col-md-8"> 
                        <div className="login-side-bg" style={bgStyle}> </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                        <div className="login-user-input">
                            <h1>Uva Holder</h1>
                            <Input 
                                size="large" 
                                placeholder="User Name" 
                                prefix={<UserOutlined />} 
                                onChange={(e) => setUserName(e.target.value)} 
                            />
                            <Button 
                                type="primary" 
                                size="large" block 
                                onClick={submitUserName} > 
                            Login 
                            </Button>

                        </div>
                    </div>
                </div> 
            </div>
        </React.Fragment>
    );

};


export default Login;