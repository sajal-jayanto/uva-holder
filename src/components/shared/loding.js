import React from 'react';

import { Spin } from 'antd';

const Loding = () => {

    return(
        <React.Fragment>
            <Spin tip="Loading..."/>
        </React.Fragment>
    );
};

export default Loding;