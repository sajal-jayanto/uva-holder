import React from 'react'
import { connect } from 'react-redux';

import { Column } from "@ant-design/charts";
import { Line } from '@ant-design/charts';

import { Card, Col, Row } from 'antd';


const VerdictGrapg = ({ userName, userUName , verdictCount , lineGraphData }) => {

    const verdictCountConfig = {
        
        padding : 35,
        data : [
            {
                Verdict: 'AC',
                Count: verdictCount.AC,
            },
            {
                Verdict: 'PE',
                Count: verdictCount.PE,
            },
            {
                Verdict: 'WA',
                Count: verdictCount.WA,
            },
            {
                Verdict: 'TLE',
                Count: verdictCount.TLE,
            },
            {
                Verdict: 'ML',
                Count: verdictCount.ML,
            },
            {
                Verdict: 'CE',
                Count: verdictCount.CE,
            },
            {
                Verdict: 'RE',
                Count: verdictCount.RE,
            },
            {
                Verdict: 'OL',
                Count: verdictCount.OL,
            },
        ],
        xField: 'Verdict',
        yField: 'Count',
        color: (item) => {
            switch(item.Verdict){
                case 'AC':
                    return '#228B22';
                case 'PE':
                    return '#666600';
                case 'WA':
                    return '#FF0000';
                case 'TLE':
                    return '#0000FF';
                case 'ML':
                    return '#0000AA';
                case 'CE':
                    return '#AAAA00';
                case 'RE':
                    return '#00CED1';
                case 'OL':
                    return '#000000';
                default :
                    return '#FFFDF';
            }
        },
        label: {
            position: 'middle',
            style: {
              fill: '#FFFFFF',
              opacity: 1,
            },
        },
        
    };
    
    const lineGraphDataConfig = {
        padding : 35,
        data: lineGraphData,
        xField: 'key',
        yField: 'solved',
        stepType: 'vh',
    };

    return (
        <div className="site-card-wrapper">
            <Row gutter={[32, 8]}>
                <Col span={12} > <Card size='small' bordered={true}>  <Column { ...verdictCountConfig } /> </Card> </Col>
                <Col span={12} > <Card size='small' bordered={true}> <Line { ...lineGraphDataConfig } />  </Card> </Col>
            </Row>
        </div>
    )
}

export default VerdictGrapg;
