import React , { useEffect } from 'react';
import { userSubmissionsAction } from '../../redux/userSubmissions/userSubmissionsAction';
import Loding from '../shared/loding';
import { useDispatch } from 'react-redux';
import { connect } from 'react-redux';
import VerdictGrapg from './VerdictGrapg';
import { Carousel } from 'antd';


const Dashbord = ({ submissionsResponse , problemResponse}) => {

    const dispatch = useDispatch();

    console.log(problemResponse);

    useEffect(() => {

        const userId = localStorage.getItem('userId');
        const fatchData = async () => {
            await userSubmissionsAction(dispatch , userId);
        } 
       
        fatchData();

    }, [dispatch]);

    const contentStyle = {
        height: '300px',
        color: '#fff',
        lineHeight: '160px',
        textAlign: 'center',
        background: '#364d79',
    };

    return(
        <React.Fragment>
            {submissionsResponse.loding || !submissionsResponse.submissions.verdictCount ? (<div>vcbcvbcv</div>) : (
                <>
                <Carousel autoplay>
                    <div>
                        <h3 style={contentStyle}>1</h3>
                    </div>
                    <div>
                        <h3 style={contentStyle}>2</h3>
                    </div>
                    <div>
                        <h3 style={contentStyle}>3</h3>
                    </div>
                    <div>
                        <h3 style={contentStyle}>4</h3>
                    </div>
                </Carousel>

                <VerdictGrapg  
                    userName={submissionsResponse.submissions.name}
                    userUName={submissionsResponse.submissions.uname}
                    verdictCount={submissionsResponse.submissions.verdictCount}
                    lineGraphData={submissionsResponse.submissions.eachYearSolveCount}
                />
                </>
            )}
        </React.Fragment>
    );
};


const mapStateTopProps = state => { 
    return {
        submissionsResponse : state.SubmissionData,
        problemResponse : state.allProblemData
    };
}

export default connect(mapStateTopProps , null)(Dashbord);




















/*


import React from 'react'




import { Card, Col, Row } from 'antd';


const VerdictGrapg = ( { verdictCount , userName , userUname , lineGraphData} ) => {

    const data = [
        {
            Verdict: 'AC',
            Count: verdictCount.accepted,
        },
        {
            Verdict: 'PE',
            Count: verdictCount.presentationError,
        },
        {
            Verdict: 'WA',
            Count: verdictCount.wrongAnswer,
        },
        {
            Verdict: 'TL',
            Count: verdictCount.timeLimit,
        },
        {
            Verdict: 'ML',
            Count: verdictCount.memoryLimit,
        },
        {
            Verdict: 'CE',
            Count: verdictCount.compileError,
        },
        {
            Verdict: 'RE',
            Count: verdictCount.runtimeError,
        },
        {
            Verdict: 'OT',
            Count: verdictCount.outputLimit,
        },
        
    ];
    const config = {
        title: {
          visible: true,
          text: 'Problem Submission Statistics',
        },
        description: {
          visible: true,
          text: `${userName} - ${userUname}`,
        },
        data,
        xField: 'Verdict',
        yField: 'Count',
        colorField: 'Verdict',
        color: ['#228B22', '#666600', '#FF0000', '#0000FF', '#0000AA', '#AAAA00', '#00CED1', '#000000'],
        label: {
            visible: true,
            position: 'top',
            adjustColor: true,
        },
        
    };

    const configline = {
        title: {
          visible: true,
          text: 'Progress Over The Years',
        },
        description: {
          visible: true,
          text: `${userName} - ${userUname}`,
        },
        padding: 'auto',
        forceFit: true,
        data : lineGraphData,
        xField: 'year',
        yField: 'solve',
        smooth: false,
        label: {
            visible: true,
            type: 'point',
        },
        point: {
            visible: true,
            size: 2,
            shape: 'diamond',
            style: {
                fill: 'white',
                stroke: '#2593fc',
                lineWidth: 2,
            },
        },
    };


    return (
        <div className="site-card-wrapper">
            <Row gutter={[32, 8]}>
                <Col span={12} > <Card size='small' bordered={false}> <ColumnChart { ...config } /> </Card> </Col>
                <Col span={12} > <Card size='small' bordered={false}> <StepLine { ...configline } />  </Card> </Col>
            </Row>
        </div>
    )
}

export default VerdictGrapg
*/