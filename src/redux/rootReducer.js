import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import loginReducer from './login/loginReducer';
import userSubmissionsReducer from './userSubmissions/userSubmissionsReducer';
import liveSubmissionsReducer from './liveSubmissions/liveSubmissionsReducer';

const persistConfig = {
    key: 'root',
    storage,
    reduxStore:['loginReducer' , 'userSubmissionsReducer' , 'liveSubmissionsReducer']
}

const rootReducer = combineReducers({
    loginData : loginReducer,
    SubmissionData : userSubmissionsReducer,
    liveSubmissionsData : liveSubmissionsReducer
});

export default persistReducer(persistConfig , rootReducer);
