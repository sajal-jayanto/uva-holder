import { LOGIN_REQUEST_SUBMIT,LOGIN_REQUEST_SUCCESS,LOGIN_REQUEST_ERROR } from './loginType';
import http from '../../util/axios.config';

export const loginAction = async (dispatch , userName) => {

    //console.log(userName);
    try {
        dispatch({ type : LOGIN_REQUEST_SUBMIT });

        const res = await http.get(`uname2uid/${userName}`);
        // console.log(res.data);
        
        const userId = Number(res.data);

        if(userId !== 0){
            
            dispatch({ type : LOGIN_REQUEST_SUCCESS , payload : userId });
            localStorage.setItem("userId" ,  userId );
            localStorage.setItem("userName" , userName);
            return true;
        }   
        else {
            dispatch({ type : LOGIN_REQUEST_ERROR });
            return false;
        }
    }
    catch(error){
        dispatch({ type : LOGIN_REQUEST_ERROR });
        return false;
    }
}