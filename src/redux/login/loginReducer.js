import { LOGIN_REQUEST_SUBMIT,LOGIN_REQUEST_SUCCESS,LOGIN_REQUEST_ERROR } from './loginType';


const initialState = {

    userId : null,
    loding : false,
    error : false

};

const loginReducer = (state = initialState , action) => {

    switch(action.type){
        case LOGIN_REQUEST_SUBMIT  : {
            
            const newState = { ...state };
            
            newState.loding = true;
            newState.error = false;

            return newState;

        }
        case LOGIN_REQUEST_SUCCESS : {

            const newState = { ...state };

            newState.loding = false;
            newState.userId = action.payload;
            newState.error = false;

            return newState;

        }
        case LOGIN_REQUEST_ERROR : {
            
            const newState = { ...state };

            newState.loding = false;
            newState.error = true;
            
            return newState;

        }
        default : {
            return state;
        }
    }

}

export default loginReducer;