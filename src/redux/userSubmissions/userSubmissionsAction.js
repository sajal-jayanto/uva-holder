import { REQUEST_FOR_SUBMISSIONS_DATA , REQUEST_FOR_SUBMISSIONS_DATA_SUCCESS , REQUEST_FOR_SUBMISSIONS_DATA_ERROR } from './userSubmissionsType';
import { userSubmissionDataDecode } from '../../util/apiDataDecoder';
import http from '../../util/axios.config';



export const userSubmissionsAction = async (dispatch , userId) => {

    try {
        
        dispatch({ type : REQUEST_FOR_SUBMISSIONS_DATA });

        const res = await http.get(`subs-user/${userId}`);
        const data = await userSubmissionDataDecode(res.data);
        
        dispatch({ type : REQUEST_FOR_SUBMISSIONS_DATA_SUCCESS , payload : data });
        
        return true;

    }
    catch(error){
        
        dispatch({ type : REQUEST_FOR_SUBMISSIONS_DATA_ERROR });
        return false;
    }
}

