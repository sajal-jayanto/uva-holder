import { REQUEST_FOR_SUBMISSIONS_DATA , REQUEST_FOR_SUBMISSIONS_DATA_SUCCESS , REQUEST_FOR_SUBMISSIONS_DATA_ERROR } from './userSubmissionsType';

const initialState = {

    loding : false,
    submissions : {},
    error : false

};


const userSubmissionsReducer = (state = initialState , action) => {

    switch(action.type){
        
        case REQUEST_FOR_SUBMISSIONS_DATA : {

            const newState = { ...state };

            newState.loding = true;
            newState.error = false;

            return newState;

        }
        case REQUEST_FOR_SUBMISSIONS_DATA_SUCCESS : {

            const newState = { ...state };

            newState.loding = false;
            newState.submissions = action.payload;
            newState.error = false;

            return newState;

        }
        case REQUEST_FOR_SUBMISSIONS_DATA_ERROR : {

            const newState = { ...state };

            newState.loding = false;
            newState.error = true;

            return newState;

        }
        default : {
            return state;
        }
    }
}

export default userSubmissionsReducer;