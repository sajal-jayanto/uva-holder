import { 
    
    REQUEST_FOR_ALL_PROBLEM_LIST,
    REQUEST_FOR_ALL_PROBLEM_LIST_SUCCESS,
    REQUEST_FOR_ALL_PROBLEM_LIST_ERROR

} from './allProblemListType';

const initialState = {
    loding : false,
    problemlist : null,
    error : false
};

const allProblemListReducer = (state = initialState , action) => {

    switch(action.type){

        case REQUEST_FOR_ALL_PROBLEM_LIST : {

            console.log(state)
            const newState = { ...state };

            newState.loding = true;
            newState.error = false;

            return newState;

        }
        case REQUEST_FOR_ALL_PROBLEM_LIST_SUCCESS : {

            const newState = { ...state };

            newState.loding = false;
            newState.problemlist = action.payload;
            newState.error = false;

            return newState;
        }
        case REQUEST_FOR_ALL_PROBLEM_LIST_ERROR : {

            const newState = { ...state };

            newState.loding = false;
            newState.error = true;

            return newState;
        }
        default:
            return state;
    }

}

export default allProblemListReducer;
