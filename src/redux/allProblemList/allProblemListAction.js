import { 
    
    REQUEST_FOR_ALL_PROBLEM_LIST,
    REQUEST_FOR_ALL_PROBLEM_LIST_SUCCESS,
    REQUEST_FOR_ALL_PROBLEM_LIST_ERROR

} from './allProblemListType';

import http from '../../util/axios.config';


export const allProblemlListAction = async (dispatch) => {

    try{

        dispatch({ type : REQUEST_FOR_ALL_PROBLEM_LIST });

        const res = await http.get(`p`);
        const data = res.data;

        localStorage.setItem("allProblemList" , JSON.stringify(data));
        dispatch({ type : REQUEST_FOR_ALL_PROBLEM_LIST_SUCCESS});

        return true;

    }
    catch(error){

        dispatch({ type : REQUEST_FOR_ALL_PROBLEM_LIST_ERROR });
        return false;
    }


}