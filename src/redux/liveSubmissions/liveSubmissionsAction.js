import {  
    REQUEST_FOR_LIVE_SUBMISSIONS_DATA, 
    REQUEST_FOR_LIVE_SUBMISSIONS_DATA_SUCCESS,
    REQUEST_FOR_LIVE_SUBMISSIONS_DATA_ERROR

} from './liveSubmissionsType';

import http  from '../../util/axios.config';
import { findProblemByProblemId } from '../../util/apiDataDecoder';

export const liveSubmissionsAction = async (dispatch , lastPollId) => {

    try{

        dispatch({ type : REQUEST_FOR_LIVE_SUBMISSIONS_DATA });

        const allProblemsList = JSON.parse(localStorage.getItem("allProblemList"));

        const res = await http.get(`poll/${lastPollId}` , { timeout: 60000 });
        const data = res.data;
        
        let liveData = [];
        let len = data.length;
        let newLastPullId = lastPollId;

        if(len >= 1){

            newLastPullId = data[len - 1].id;

            for(let sub of data){

                let pos = -1 , low = 0, high = liveData.length - 1;

                while(low <= high){    
                    
                    if(sub.msg.sid == liveData[low].key) { pos = low ; break; }
                    low++;
                }
                let position = findProblemByProblemId(sub.msg.pid);

                let updateSubmission = {
                        
                    key : sub.msg.sid,
                    submissionsId : sub.msg.sid,
                    numberWithName : `${allProblemsList[position][1]} - ${allProblemsList[position][2]}`,
                    userName : `${sub.msg.name}(${sub.msg.uname})`,
                    verdict : sub.msg.ver,
                    language : sub.msg.lan,
                    runtime : (sub.msg.run / 1000).toFixed(3),
                    bestRuntime : (allProblemsList[position][4] / 1000).toFixed(3),
                    rank : sub.msg.rank !== -1 ? sub.msg.rank : '-',
                    submissionTime : sub.msg.sbt
                }

                if(pos === -1){

                    liveData.unshift(updateSubmission);
                    if(liveData.length >= 100) liveData.pop();
                }
                else liveData[pos] = updateSubmission;
                
            }
        }
        
        dispatch({ type : REQUEST_FOR_LIVE_SUBMISSIONS_DATA_SUCCESS , payload : liveData });
        
        return newLastPullId;
    }
    catch(error){

        dispatch({ type : REQUEST_FOR_LIVE_SUBMISSIONS_DATA_ERROR });
    }
}


