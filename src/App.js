import React from "react";
import './App.css';
import Routers, {  } from './routes/routes';

function App() {
    return (
        <React.Fragment>
            <Routers />
        </React.Fragment>
    );
}

export default App;
