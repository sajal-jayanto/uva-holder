import React from 'react';

import { Layout } from "antd";
import '../styles/layout.css';

import MySidber from '../components/layouts/sidber'
import MyHeader from '../components/layouts/Header'
import MyFooter from '../components/layouts/Footer'

const { Content } = Layout;

const MyLayout = ({ children }) => {

    return(
        <Layout style={{ minHeight: "100vh" }}>

            <MySidber />
            <Layout>
                <MyHeader/>
                
                <Content className="container-fluid no-padding no-margin">
                    <div className="main-content-layout">
                        {children}
                    </div>
                </Content>
                
                <MyFooter />

            </Layout>

        </Layout>
    );
};


export default MyLayout;