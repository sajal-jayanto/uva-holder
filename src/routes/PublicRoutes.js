import React from 'react';
import { Route } from 'react-router-dom';
import DashbordPage from '../pages/dashbord';

const PublicRoute = ({ component: Component, restricted, ...rest }) => (
    <Route 
    {...rest} 
    render={props => {
            if(localStorage.hasOwnProperty("userId")){ 
                return <DashbordPage />
            }else {
                return <Component {...props} />
            } 
        }
    }
    />
)

export default PublicRoute;