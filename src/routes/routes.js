import React from 'react';
import { BrowserRouter , Switch  } from 'react-router-dom';

import LoginPage from '../pages/login';
import DashbordPage from '../pages/dashbord';
import RankListPage from '../pages/rankList';
import CpBooksPage from '../pages/cpBooks';
import ContestPage from '../pages/contests';
import ComparePage from '../pages/compare';
import ProblemsSearchPage from '../pages/problems/search';
import ProblemsListPage from '../pages/problems/problemlist';
import LiveSubmissionsPage  from '../pages/liveSubmissions';

import PrivateRoute from '../routes/PrivateRoutes';
import PublicRoute from '../routes/PublicRoutes';


const Routers = () => {

    return(
        <BrowserRouter>
            <Switch>

                <PublicRoute exact restricted={true}  path="/login" component={LoginPage} />
                <PrivateRoute path="/dashboard" component={DashbordPage} />  
                <PrivateRoute path="/live-submissions" component={LiveSubmissionsPage} />
                <PrivateRoute path="/rank-list" component={RankListPage} />
                <PrivateRoute exact path="/problems" component={ProblemsListPage} />
                <PrivateRoute path="/problems/search" component={ProblemsSearchPage} />
                <PrivateRoute path="/cp-book" component={CpBooksPage} />
                <PrivateRoute path="/contest" component={ContestPage} />
                <PrivateRoute path="/compare" component={ComparePage} />  

            </Switch>
        </BrowserRouter>
    );
};

export default Routers;