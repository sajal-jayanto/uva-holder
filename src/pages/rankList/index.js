import React from 'react'
import MyLayout from '../../layouts'

const RankListPage = () => {

    return (
        <MyLayout>
            Ranklist
        </MyLayout>
    );
}

export default RankListPage;