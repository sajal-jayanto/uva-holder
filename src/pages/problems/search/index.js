import React from 'react';
import MyLayout from '../../../layouts';
import ProblemsSearch from '../../../components/problems/search';


const ProblemsSearchPage = () => {

    return (   
        <MyLayout>
            <ProblemsSearch/>
        </MyLayout>
    );

};

export default ProblemsSearchPage;