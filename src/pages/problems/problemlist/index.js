import React from 'react';
import MyLayout from '../../../layouts';
import ProblemsList from '../../../components/problems/problemlist';


const ProblemsListPage = () => {

    return (
        <MyLayout>
            <ProblemsList/>
        </MyLayout> 
    );

};

export default ProblemsListPage;