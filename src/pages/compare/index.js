import React from 'react'
import MyLayout from '../../layouts'

const ComparePage = () => {

    return (
        <MyLayout>
            Compare
        </MyLayout>
    );
}

export default ComparePage;