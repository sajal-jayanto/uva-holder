import React from 'react'
import MyLayout from '../../layouts'

const ContestPage = () => {

    return(
        <MyLayout>
            Contest
        </MyLayout>
    );

}

export default ContestPage;