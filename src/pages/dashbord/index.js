import React from 'react';
import Dashbord from '../../components/dashbord';
import MyLayout from '../../layouts';


const DashbordPage = () => {

    return (
        
        <MyLayout>
           <Dashbord />
        </MyLayout>
    );

};

export default DashbordPage;