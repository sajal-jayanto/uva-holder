import React from 'react';
import LiveSubmissions from '../../components/liveSubmissions';
import MyLayout from '../../layouts';

const LiveSubmissionsPage = () => {

    return (
        
        <MyLayout>
            <LiveSubmissions/>
        </MyLayout>    

    );

};

export default LiveSubmissionsPage;